package cc.vlab.controller;

import cc.vlab.SearchResponse;
import cc.vlab.logging.LogMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {

    @LogMessage("Search request")
    @RequestMapping(value = "/api/v1/search", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    public SearchResponse search(@RequestBody String query) {
        return new SearchResponse(10);
    }
}
