package cc.vlab;

public class Document {

    private String id;

    public Document() {
    }

    public Document(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
