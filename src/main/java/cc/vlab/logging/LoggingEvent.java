package cc.vlab.logging;

import net.logstash.logback.argument.StructuredArgument;
import org.slf4j.event.Level;

import java.util.ArrayList;
import java.util.List;

public class LoggingEvent {

    private Level level = Level.INFO;

    private String message = "";

    private List<StructuredArgument> args = new ArrayList<>();

    private Throwable exception;

    public String getMessage() {
        return message;
    }

    public LoggingEvent setMessage(String message) {
        this.message = message;
        return this;
    }

    public Level getLevel() {
        return level;
    }

    public LoggingEvent setLevel(Level level) {
        this.level = level;
        return this;
    }

    public List<StructuredArgument> getArgs() {
        return args;
    }

    public LoggingEvent add(StructuredArgument arg) {
        args.add(arg);
        return this;
    }

    public Throwable getException() {
        return exception;
    }

    public LoggingEvent setException(Throwable throwable) {
        this.exception = throwable;
        return this;
    }

    public boolean hasException() {
        return exception != null;
    }
}
