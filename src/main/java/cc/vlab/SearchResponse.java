package cc.vlab;

public class SearchResponse {

    private int hits;

    public SearchResponse() {
    }

    public SearchResponse(int hits) {
        this.hits = hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public int getHits() {
        return hits;
    }
}
