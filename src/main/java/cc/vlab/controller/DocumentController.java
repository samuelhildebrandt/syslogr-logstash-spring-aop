package cc.vlab.controller;

import cc.vlab.Document;
import cc.vlab.logging.LogMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class DocumentController {

    @LogMessage("Creating document")
    @RequestMapping(value = "/api/v1/collections/{collectionId}/documents", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Document create(@PathVariable String collectionId, @RequestBody Document document) {
        return document;
    }

    @LogMessage("Reading document")
    @RequestMapping(value = "/api/v1/collections/{collectionId}/documents/{documentId}", method = RequestMethod.GET)
    public Document read(@PathVariable String collectionId, @PathVariable String documentId) {
        return new Document(UUID.randomUUID().toString());
    }

    @LogMessage("Deleting document")
    @RequestMapping(value = "/api/v1/collections/{collectionId}/documents/{documentId}", method = RequestMethod.DELETE)
    public Document delete(@PathVariable String collectionId, @PathVariable String documentId) throws Exception {
        throw new Exception("Error deleting document");
    }
}
