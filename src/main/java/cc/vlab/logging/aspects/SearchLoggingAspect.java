package cc.vlab.logging.aspects;

import cc.vlab.SearchResponse;
import cc.vlab.logging.LoggingEvent;
import org.apache.commons.codec.Charsets;
import org.apache.http.client.utils.URLEncodedUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import static net.logstash.logback.argument.StructuredArguments.value;

@Aspect
@Component
public class SearchLoggingAspect extends ControllerLoggingAspect {

    @Around("within(cc.vlab.controller.SearchController) && args(query)")
    public Object logSearch(ProceedingJoinPoint joinPoint, String query) throws Throwable {
        LoggingEvent event = newLoggingEvent(joinPoint)
                .add(value("query", URLEncodedUtils.parse(query, Charsets.UTF_8)));

        try {
            SearchResponse response = (SearchResponse) proceed(joinPoint, event);
            event.add(value("hits", response.getHits()));
            return response;
        } finally {
            publish(event);
        }
    }
}
