package cc.vlab.logging.aspects;

import cc.vlab.Document;
import cc.vlab.logging.LoggingEvent;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import static net.logstash.logback.argument.StructuredArguments.value;

@Aspect
@Component
public class ReportLoggingAspect extends ControllerLoggingAspect {

    @Around("within(cc.vlab.controller.DocumentController) && args(collectionId, documentId)")
    public Object logDocument(ProceedingJoinPoint joinPoint, String collectionId, String documentId) throws Throwable {
        LoggingEvent event = newLoggingEvent(joinPoint)
                .add(value("collectionId", collectionId))
                .add(value("documentId", documentId));

        try {
            return proceed(joinPoint, event);
        } finally {
            publish(event);
        }
    }

    @Around("within(cc.vlab.controller.DocumentController) && args(collectionId, document)")
    public Object logDocument(ProceedingJoinPoint joinPoint, String collectionId, Document document) throws Throwable {
        return logDocument(joinPoint, collectionId, document.getId());
    }
}
