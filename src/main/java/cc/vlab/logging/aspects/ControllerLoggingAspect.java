package cc.vlab.logging.aspects;

import cc.vlab.logging.LogMessage;
import cc.vlab.logging.LoggingEvent;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.util.Optional;

import static net.logstash.logback.argument.StructuredArguments.value;

public abstract class ControllerLoggingAspect {

    private static final Logger log = LoggerFactory.getLogger(ControllerLoggingAspect.class);

    public Object proceed(ProceedingJoinPoint joinPoint, LoggingEvent event) throws Throwable {
        try {
            return joinPoint.proceed();
        } catch (Throwable e) {
            event.setLevel(Level.ERROR)
                    .setException(e)
                    .add(value("exceptionClass", e.getClass().getName()))
                    .add(value("exceptionMessage", e.getMessage()));

            throw e;
        }
    }

    public LoggingEvent newLoggingEvent(ProceedingJoinPoint joinPoint) {
        return new LoggingEvent()
                .setMessage(getLogMessage(joinPoint))
                .add(value("httpMethod", getHttpServletRequest().getMethod()))
                .add(value("httpSession", getHttpSession().getId()))
                .add(value("httpServletPath", getHttpServletRequest().getServletPath()))
                .add(value("javaClass", joinPoint.getTarget().getClass().getName()))
                .add(value("javaMethod", joinPoint.getSignature().getName()))
                .add(value("controllerMapping", getRequestMapping(joinPoint)));
    }

    public void publish(LoggingEvent loggingEvent) {
        switch (loggingEvent.getLevel()) {
            case ERROR:
                log.error(loggingEvent.getMessage(), loggingEvent.getArgs().toArray());
                break;
            case WARN:
                log.warn(loggingEvent.getMessage(), loggingEvent.getArgs().toArray());
                break;
            case INFO:
                log.info(loggingEvent.getMessage(), loggingEvent.getArgs().toArray());
                break;
            case DEBUG:
                log.debug(loggingEvent.getMessage(), loggingEvent.getArgs().toArray());
                break;
            case TRACE:
                log.trace(loggingEvent.getMessage(), loggingEvent.getArgs().toArray());
        }
    }

    public String getLogMessage(ProceedingJoinPoint joinPoint) {
        return getMethodAnnotation(joinPoint, LogMessage.class).map(LogMessage::value)
                .orElse(joinPoint.getTarget().getClass().getName() + "#" + joinPoint.getSignature().getName());
    }

    public String[] getRequestMapping(ProceedingJoinPoint joinPoint) {
        return getMethodAnnotation(joinPoint, RequestMapping.class).map(RequestMapping::value).orElse(new String[0]);
    }

    private <T extends Annotation> Optional<T> getMethodAnnotation(ProceedingJoinPoint joinPoint, Class<T> annotationClass) {
        return Optional.ofNullable(
                ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(annotationClass));
    }

    private static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
    }

    private static HttpSession getHttpSession() {
        return (HttpSession) RequestContextHolder.currentRequestAttributes().resolveReference(RequestAttributes.REFERENCE_SESSION);
    }
}
